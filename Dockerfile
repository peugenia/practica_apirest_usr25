# Imagen raíz
FROM node

# Carpeta raíz
WORKDIR /apitechu

# Copia de archivos
ADD . /apitechu

# Añadir volumen
VOLUME ['/logs']

# Exponer puerto
EXPOSE 3000

# Instalar las dependencias
#RUN npm install

# Comando de inicialización
CMD ["npm","start"]
