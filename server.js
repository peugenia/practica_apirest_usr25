var express = require('express');
var app = express();
var bodyParser = require('body-parser');
//modificar el función del tipo del body de una petición. Ej. text(), json()
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMlabURL = "https://api.mlab.com/api/1/databases/apitechpau/collections/";
var mLabAPIKey = "apiKey=PKstYxVOxnpsxiz3LViO6fodD2g7euDt";
var requestJson = require('request-json');

app.listen(port);
console.log("API esuchando en el puerto PROBANDOOO " + port);

//definimos una ruta de bienvenida.
/*
app.get('/apitechu/v1',
  function(req,res) {
    console.log("GET /apitechu/v1");

    res.send(
      {
        "msg" : "Bienvenido a la API de Tech University Molona"
      }
    )
  }
);
*/

//preparamos otra ruta.
/*app.get('/apitechu/v1/users',
  function(req,res){
    console.log("GET /apitechu/v1/users");
    //root -> indica la ruta. __dirname tiene el valor del directorio donde se ejecuta el script
    res.sendFile('usuarios.json',{root: __dirname});
  }

);

/*
* fecha : 20-03-2018
* GET
* creamos consulta sobre la bd mongo
*/
/*
app.get('/apitechu/v2/users',
  function(req,res){
    console.log("GET /apitechu/v2/users");
    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");
    console.log("URL ->" + "user?" + mLabAPIKey);
    httpClient.get("user?" + mLabAPIKey,
      function(err,resMLab,body){
        var response = !err ? body :{
          "msg" : "Error obteniendo usuarios."
        }
        res.send(response);
      }
    );
  }
);
*/

/*
* fecha : 20-03-2018
* GET por id
* creamos consulta sobre la bd mongo
*/
app.get('/apitechu/v2/users/:id',
  function(req,res){
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id" : ' + id + '}'

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("user?" + query + "&"+   mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        };
        res.status(500);
      } else {
          if (body.length > 0) {
              response = body[0];
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            };
          res.status(404);
          }
      }
      res.send(response);
    }
    );
  }
);

/*
* fecha : 27-03-2018
* GET por id en la colección account
* creamos consulta sobre la bd mongo
*/
app.get('/apitechu/v2/users/:id/accounts',
  function(req,res){
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"userId" : ' + id + '}'

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("account?" + query + "&"+   mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        };
        res.status(500);
      } else {
          if (body.length > 0) {
              response = body;
          } else {
            response = {
              "msg" : "Usuario no encontrado."
            };
          res.status(404);
          }
      }
      res.send(response);
    }
    );
  }
);

/*
* fecha : 27-03-2018
* GET por id en la colección movement
* creamos consulta sobre la bd mongo
*/
app.get('/apitechu/v2/users/account/:id/movements',
  function(req,res){
    console.log("GET /apitechu/v2/users/account/:id/movements");

    var id = req.params.id;
    var query = 'q={"accountId" : ' + id + '}&s={"fecha":1}'

    httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");

    httpClient.get("movement?" + query + "&"+   mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        response = {
          "msg" : "Error obteniendo movimientos."
        };
        res.status(500);
      } else {
          if (body.length > 0) {
              response = body;
          } else {
            response = {
              "msg" : "No hay movimientos."
            };
          res.status(404);
          }
      }
      res.send(response);
    }
    );
  }
);

app.post('/apitechu/v1/users',
  function(req,res){
    console.log("POST /apitechu/v1/users");
    console.log(req);

    //construimos un nuevo objeto json.
    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "country" : req.body.country
    };

    //mostramos en consola los valores recuperados
    console.log("first_name " + req.body.first_name);
    console.log("last_name " + req.body.last_name);
    console.log("country " + req.body.country);

    //estamos cogiendo el fichero y js lo trata como un array
    var users = require('./usuarios.json');
    users.push(newUser);

    writeUserDataToFile(users);

/*    var fs = require('fs');
    var jsonUserData = JSON.stringify(users);

    fs.writeFile(
        "./usuarios.json",
        jsonUserData,
        "utf8",
        function(err){
          if(err){
            console.log(err);
          } else{
            console.log("Usuario persistido");
          }
        }
    )*/
    //console.log("Usuario añadido con éxito");
    //res.send(users);
    res.send(
      {
        "msg" : "Usuario añadido con éxito"
      }
    );
  }

);

//borramos un elemento
app.delete("/apitechu/v1/users/:id",
  function(req,res){
    console.log("DELETE /apitechu/v1/users/:id");

    var users = require('./usuarios.json');
    //coge el id que le hemos pasado -1 y nos quedamos con 1.
    users.splice(req.params.id -1,1);
    writeUserDataToFile(users);

    res.send(
      {
        "msg" : "Usuario borrado con éxito"
      }
    );
  }

)


app.post('/apitechu/v1/monstruo/:p1/:p2',
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("headers");
    console.log(req.headers);

    console.log("Body");    //get no lo tiene.
    console.log(req.body);

    res.send(
      {
        "msg" : "Ya lo tengo"
      }
    )

  }
);

//Método POST para hacer Login
app.post('/apitechu/v1/login',
  function(req,res){
    console.log("POST /apitechu/v1/login");

    //Construimos un objeto json con los datos de entrada
    var userLogin = {
      "email" : req.body.email,
      "password" : req.body.password
    };

    //Mostramos en consola los valores recuperados
    console.log("email " + req.body.email);
    console.log("password " + req.body.password);

    //Estamos cogiendo el fichero y js lo trata como un array
    var users = require('./usuariosLogin.json');

    var msgSal = {
      "msg" : "Login Incorrecto"
    };

    for (user of users) {
        if (user.email == userLogin.email){
            if(user.password == userLogin.password){
              console.log("email y password validados");
              user.logged = true;
              msgSal.msg = "Login correcto";
              msgSal.id = user.id;
              console.log("Logged " + user.logged);
              writeUserDataToFile(users);
            }else{
              console.log("Password incorrecta");
              msgSal.msg = "Password incorrecta";
            }
            break;
        }
    }

    res.send(msgSal);

  }
);

/*
* 20-03-2018
* Login contra Mongo
*/
app.post('/apitechu/v2/login',
  function(req,res){

    console.log("POST /apitechu/v2/login");
    //Construimos un objeto json con los datos de entrada
    var userLogin = {
      "email" : req.body.email,
      "password" : req.body.password
    };

    //Mostramos en consola los valores recuperados
    console.log("Usuario:");
    console.log("- email :" + req.body.email);
    console.log("- password : " + req.body.password);

    var query = 'q={"email":"' + userLogin.email + '","password":"' + userLogin.password +'"}';
    console.log("Query login : " + query);
    var httpClient = requestJson.createClient(baseMlabURL);

    console.log("Cliente creado");
    var response = {"msg" : "mensaje salida"};
    httpClient.get("user?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
      var encontrado = false;
      if (err) {
        //response = {"msg" : "Error obteniendo usuario."};
        response.msg = "Error obteniendo usuario.";
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          encontrado = true;
          //response = {"msg" : "GET - LOGIN CORRECTO."};
          response.msg = "GET - LOGIN CORRECTO.";
          response.user = body[0];

        } else {
          //response = {"msg" : "Usuario no encontrado."};
          response.msg = "Usuario no encontrado.";
          res.status(404);
          res.send(response);
        }
      }
      console.log("Valor var reponse : " + JSON.stringify(response));
       if(encontrado){

         query = 'q={"id":'+ body[0].id + '}';
         var putBody = '{"$set":{"logged":true}}';

         httpClient.put("user?" + query + "&"+   mLabAPIKey,JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
           if (errPUT) {
             //response = {"msg" : "Error al hacer LOGIN."};
             response.msg = "Error al hacer LOGIN.";
             res.status(500);
           } else {
             //response = {"msg" : "PUT - Login correcto."};
             response.msg = "PUT - Login correcto.";
             console.log("PUT OK - Valor var reponse : " + JSON.stringify(response));
           }
           res.send(response);
         });
       }
       console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
       //res.send(response);
       }
     );
 });
/*
* 20-03-2018
* Login contra Mongo
*/
app.post('/v2/user',
  function(req,res){

    console.log("POST /v2/user");
    console.log("Datos de entrada del nuevo usuario:");
    console.log("- first_name : " + req.body.first_name);
    console.log("- last_name : " + req.body.last_name);
    console.log("- email :" + req.body.email);
    console.log("- password : " + req.body.password);

    var httpClient = requestJson.createClient(baseMlabURL);

    //Comprobamos si existe algún usuario con el mismo email.
    var query = 'q={"email":"' + req.body.email + '"}';
    console.log("Query consulta -> " + query);
    console.log("Cliente creado");
    var response = {"msg" : "mensaje salida"};

    httpClient.get("user?" + query + "&"+   mLabAPIKey,

      function(err, resMLab, body){
      var encontrado = true;
      if (err) {
        response.msg = "Error obteniendo usuario.";
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          response.msg = "GET - USUARIO EXISTENTE";
          res.status(404);
          res.send(response);
        } else {
          encontrado = false;
          response.msg = "GET - USUARIO NO ENCONTRADO";
        }
      }
      console.log("Valor var reponse : " + JSON.stringify(response));
      if(!encontrado){
        console.log("Buscamos el último usuario dado de alta");
        query = 's={"id":-1}';
        httpClient.get("user?" + query + "&"+   mLabAPIKey,
        function(err, resMLab, body){
          var maxuser = false;
          if (err) {
            response.msg = "Error obteniendo máximo id.";
            res.status(500);
            res.send(response);
          } else {
            if (body.length > 0) {
              response = body[0];
              maxuser = true;
            } else {
              response.msg = "GET - ULTIMO USUARIO NO ENCONTRADO";
              res.status(404);
              res.send(response);
            }
          }
          if(maxuser){
              //Damos de alta el usuario
              var newid = response.id + 1;
              var postBody = '{"id":'+ newid +',"first_name":"'+ req.body.first_name +'","last_name": "'+ req.body.last_name+'","email": "'+req.body.email +'","password": "'+ req.body.password +'"}';
              console.log("postBody - " +postBody);
              httpClient.post("user?"+   mLabAPIKey,JSON.parse(postBody),
              function(errPOST, resMLabPOST, bodyPOST) {
                if (errPOST) {
                  response.msg = "Error al hacer ALTA.";
                  res.status(500);
                } else {
                  //response = {"msg" : "PUT - Login correcto."};
                  response = postBody;
                }
                res.send(response);
              });
            }
        });
      }
      console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
      }
    );
});

/*
* 29-04-2018
* Alta de una cuenta
*/

app.post('/v2/user/account',
  function(req,res){

    console.log("POST /v2/users/account");
    console.log("Datos de entrada del nuevo usuario:");
    console.log("- id : " + req.body.id);

    var httpClient = requestJson.createClient(baseMlabURL);
    var query = 's={"id":-1}';

    httpClient.get("account?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
        var maxaccount = false;
        if (err) {
            response.msg = "Error obteniendo máximo id.";
            res.status(500);
            res.send(response);
        } else {
            if (body.length > 0) {
              response = body[0];
              maxaccount = true;
            } else {
              response.msg = "GET - MÁXIMO ID CUENTA NO ENCONTRADO";
              res.status(404);
              res.send(response);
            }
        }
        if(maxaccount){
          console.log("Hemos recuperado el maximo id de las cuentas");
          console.log("MaxId : " + response.id);
          //Damos de alta la cuenta
          var newid = response.id + 1;
          var numAle = getRandomInt(1000000000000000,9999999999999999);
          var iban = "ES98 0182 "+numAle;
          console.log("Código iban -->" + iban);
          var postBody = '{"id":'+ newid +',"userId":'+ req.body.id +',"iban": "'+iban+'","balance": 50.01}';
          console.log("postBody - " +postBody);
          var errorAlta = false;
          httpClient.post("account?"+   mLabAPIKey,JSON.parse(postBody),
            function(errPOST, resMLabPOST, bodyPOST) {
              if (errPOST) {
                response.msg = "Error al hacer ALTA.";
                res.status(500);
                errorAlta = true;
              } else {
                //response = {"msg" : "PUT - Login correcto."};
                response = postBody;
                //res.send(response);
                //errorAlta = true;
              }
              if(errorAlta){
                console.log("Error en el alta de la cuenta");
                var deleteBody = '[]';
                var query = 'q={"id":' + req.body.id + '}';
                console.log("user?"+ query+"&"+mLabAPIKey);
                console.log("body : " +deleteBody);
                httpClient.put("user?"+ query+"&"+mLabAPIKey,JSON.parse(deleteBody),
                  function(err, resMLab, body){
                    if (err) {
                      response = {"msg" : "Error borrado usuario."};
                      res.status(500);
                    } else {
                      console.log("Borrado usuario --> " + req.body.id);
                      response = {"msg" : "Error alta cuenta. Usuario borrado"};
                    }
                    res.send(response);
                  });
              }
          });
        }
    });
});

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

/*
* 20-03-2018
* Logout contra Mongo
*/
app.post('/apitechu/v2/logout',
  function(req,res){

    console.log("POST /apitechu/v2/logout");

    //Mostramos en consola los valores recuperados
    console.log("Usuario:");
    console.log("- id :" + req.body.id);

    var query = 'q={"id":' + req.body.id + '}';

    var httpClient = requestJson.createClient(baseMlabURL);

    console.log("Cliente creado");
    var response = {"msg" : "mensaje salida"};

    httpClient.get("user?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
      var encontrado = false;
      if (err) {
        response = {"msg" : "Error obteniendo usuario."};
        res.status(500);
      } else {
        if (body.length > 0) {
          encontrado = true;
          response = {"msg" : "GET - USUARIO CORRECTO."};
          response.msg = "GET - LOGIN CORRECTO.";

        } else {
          response = {"msg" : "Usuario no encontrado."};
          res.status(404);
        }
      }

      console.log("Valor var reponse : " + JSON.stringify(response));
      if(encontrado){
        if(body[0].logged){   //Comprobamos si el usuario estaba logado.

          var putBody = '{"$unset":{"logged":""}}';

          httpClient.put("user?" + query + "&"+   mLabAPIKey,JSON.parse(putBody),
            function(errPUT, resMLabPUT, bodyPUT) {
            if (errPUT) {
              response = {"msg" : "Error al actualizar logout."};
              res.status(500);
            } else {
              response = {"msg" : "PUT - Logout correcto."};
              console.log("PUT OK - Valor var reponse : " + JSON.stringify(response));
            }
            //res.send(response);
          });
        }else{
          response = {"msg" : "Usuario correcto - No logado"};
          console.log("Usuario no logado");
        }

      }
      console.log("Antes de enviar respuesta - Valor var reponse : " + JSON.stringify(response));
      res.send(response);
      }
    );
});

//Método POST para hacer Logout. Si el usuario esta logueado ponemos logged a false,
app.post('/apitechu/v1/logout',
  function(req,res){
    console.log("POST /apitechu/v1/logout");

    //Construimos un objeto json con los datos de entrada
    var userLogged = {
      "id" : req.body.id
    };

    //Mostramos en consola los valores recuperados
    console.log("id " + req.body.id);

    //Estamos cogiendo el fichero y js lo trata como un array
    var users = require('./usuariosLogin.json');

    var msgSal = {
      "msg" : "Logout incorrecto"
    };

    for (user of users) {
        if (user.id == userLogged.id ){
              if(user.logged){
                msgSal.msg = "Logout correcto";
                msgSal.id = user.id;
                delete user.logged;
                writeUserDataToFile(users);
              }else {
                msgSal.msg = "Usuario correcto - No logueado";
              }
              break;
        }
    }

    res.send(msgSal);

  }
);


function writeUserDataToFile(data){
  var fs = require('fs');
  //para guardar el json como String y no como Object.
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(
      "./usuariosLogin.json",
      jsonUserData,
      "utf8",
      function(err){
        if(err){
          console.log(err);
        } else{
          console.log("Usuario persistido");
        }
      }
  )
};
