var mocha = require('mocha');
var chai = require('chai');           //importamos la interfaz de chai.
var chaihttp = require('chai-http');

var server = require('../server.js'); //importamos el fichero que vamos a probar.

chai.use(chaihttp);

var should = chai.should();

// los test son secuenciales.
describe('First test',
  function(){
    it('Tests that DuckDuckGo works',function(done){
        chai.request('http://www.duckduckgo.com')
        //chai.request('https://developer.mozilla.org/es/')
          .get('/')
          .end(
            function(err,res){
              console.log("Request has ended");
              console.log(err);
              res.should.have.status(200);
              done();
            }
          );
    });
  }
);

describe('Test de API Usuarios',function() {
   it('Prueba que la API de usuarios responde correctamente.',function(done) {
       chai.request('http://localhost:3000')
         .get('/apitechu/v1')
         .end(
           function(err, res) {
             res.should.have.status(200);
             res.body.msg.should.be.eql("Bienvenido a la API de Tech University Molona");
             done();
           }
         )
     }
   )
   it('Prueba que la API devuelve una lista de usuarios correctos.',
     function(done) {
       chai.request('http://localhost:3000')
       .get('/apitechu/v1/users')
       .end(
         function(err, res) {
           res.should.have.status(200);
           res.body.should.be.a("array");
           for (user of res.body) {
             user.should.have.property('email');
             user.should.have.property('password');
           }
           done();
         }
       );
     }
   );
 }
);
